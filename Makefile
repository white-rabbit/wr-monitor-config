PKGS+=wrm-rsyslog-config
PKGS+=wrm-dnsmasq-config
PKGS+=wrm-nagios-hosts-config
PKGS+=wrm-nagios-services-config
PKGS+=wrm-lldp-topology
PKGS+=wrm-cern-ldap-config

BUILD_PKG_BIN?=dpkg-buildpackage
BUILD_PKG_BIN_OPT?=-us -uc -b


WIRESHARK_VER?=""
UBUNTU_VER_NAME?=xenial
UBUNTU_VER?=16.04

.PHONY: $(PKGS) wireshark wireshark_prebuild

all: $(PKGS)
all: copy_pkgs

$(PKGS):
	cd $@ && $(BUILD_PKG_BIN) $(BUILD_PKG_BIN_OPT)

wireshark_prebuild: wireshark_download wireshark_patch

wireshark_download:
	echo "deb-src http://ch.archive.ubuntu.com/ubuntu/ $(UBUNTU_VER_NAME) multiverse universe restricted main" | $(SUDO) tee /etc/apt/sources.list.d/src.list
	echo "deb-src http://ch.archive.ubuntu.com/ubuntu/ $(UBUNTU_VER_NAME)-updates multiverse universe restricted main" | $(SUDO) tee -a /etc/apt/sources.list.d/src.list
	# to avoid crashes when installing colord
	ln -s -f /bin/true /usr/bin/chfn
	$(SUDO) apt-get update
	$(SUDO) apt-get install -y equivs devscripts
	apt-get source wireshark$(WIRESHARK_VER)
	# copy dsc file, needed for packaging (stage dpkg-genchanges)
	cp `ls wireshark_*.dsc` `ls wireshark_*.dsc | sed s/^wireshark_/wireshark-wr_/`
	cd wireshark* && $(SUDO) mk-build-deps -t "apt-get -o Debug::pkgProblemResolver=yes -y" -i debian/control

wireshark_patch:
	cd wireshark* && pwd && for i in ../patches/wireshark/0*.patch; do patch -p1 < $$i || exit 1; done
	cd wireshark* && pwd && for i in ../patches/wireshark/ubuntu_$(UBUNTU_VER)_0*.patch; do patch -p1 < $$i || exit 1; done
	sed -i 's/^wireshark (/wireshark-wr (/g' wireshark*/debian/changelog
	cd wireshark* && dch -n "Add White Rabbit support"

wireshark:
	cd wireshark* && $(BUILD_PKG_BIN) $(BUILD_PKG_BIN_OPT)

copy_pkgs:
	mkdir -p pkgs
	mv *.deb pkgs || true
	mv *.xz pkgs || true
	mv *.tar.gz pkgs || true
	mv *.dsc pkgs || true
	mv *.changes pkgs || true

