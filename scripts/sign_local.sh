#!/bin/bash

rm Packages Packages.gz Release Release.gpg InRelease
apt-ftparchive -o Dir=. packages . --no-sha1 | tee Packages | gzip >Packages.gz
apt-ftparchive -o Dir=. release . --no-sha1 > Release
gpg --clearsign -o InRelease Release
gpg -abs -o Release.gpg Release
