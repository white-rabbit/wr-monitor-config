#!/bin/bash

# special version to run apt-ftparchive on SLC6 (e.g. lxplus)
rm -f Packages Packages.gz Release Release.gpg InRelease
APT_CONFIG=../apt-archive/apt.conf ../apt-archive/ubuntu_14.04_libs/ld-linux-x86-64.so.2 --library-path ../apt-archive/ubuntu_14.04_libs ../apt-archive/apt-ftparchive -o Dir=. packages . --no-sha1 | tee Packages | gzip >Packages.gz
APT_CONFIG=../apt-archive/apt.conf ../apt-archive/ubuntu_14.04_libs/ld-linux-x86-64.so.2 --library-path ../apt-archive/ubuntu_14.04_libs ../apt-archive/apt-ftparchive -o Dir=. release . --no-sha1 > Release
echo "Following packages were found:"
cat Packages | grep -e ^Package: -e ^Version:
echo "Sign packages"
gpg --clearsign -o InRelease Release
gpg -abs -o Release.gpg Release
echo "Signing done"
