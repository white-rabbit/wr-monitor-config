#!/usr/bin/python

import sys
import subprocess
import os.path

## snmp commands
#SNMP_GET_UCNT = "/usr/lib/nagios/plugins/check_snmp -H $HOSTADDRESS$ -P 1 -C public -o 1.3.6.1.4.1.96.100.7.5.1.15.1 "
#WRPC_IP = "192.168.5.2"
SNMP_GET_UCNT = "snmpget -v 1 -c public <ip> .1.3.6.1.4.1.96.101.2.1.1.1.1.13.1"

def read_max(ip):
	snmp_ret = subprocess.check_output(SNMP_GET_UCNT.replace("<ip>", ip), shell=True)
	ucnt = snmp_ret.split(':')[1][1:-1]
	return ucnt

def main():
	if len(sys.argv) == 1:
		return

	ip = sys.argv[1]

	if len(sys.argv) == 2:
		# get ucnt from snmp, get last ucnt from file and compare
		lat = read_max(ip)
                lat_mul = int(lat) * 8 / 1000.0

	print "Latency min = " + str(lat_mul) + " us"
	sys.exit(0)



if __name__ == '__main__':
	main()
