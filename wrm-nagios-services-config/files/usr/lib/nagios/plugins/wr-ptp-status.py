#!/usr/bin/python

import sys
import subprocess
import os.path

## snmp commands
#SNMP_GET_UCNT = "/usr/lib/nagios/plugins/check_snmp -H $HOSTADDRESS$ -P 1 -C public -o 1.3.6.1.4.1.96.100.7.5.1.15.1 "
#WRPC_IP = "192.168.5.2"
SNMP_GET_UCNT = "snmpget -v 1 -c public <ip> .1.3.6.1.4.1.96.101.1.5.12.0"
FILE_UCNT = "/tmp/ucnt-<ip>"
F_UCNT_IDX = 0
F_DIFF_IDX = 1

def read_ucnt(ip):
	snmp_ret = subprocess.check_output(SNMP_GET_UCNT.replace("<ip>", ip), shell=True)
	ucnt = snmp_ret.split(':')[1][1:-1]
	return ucnt

def create_ucnt_file(ip):
	f = open(FILE_UCNT.replace("<ip>", ip), 'w')
	f.write("0;0")
	f.close()

def read_last_ucnt(ip):
	filename = FILE_UCNT.replace("<ip>", ip)
	if not os.path.isfile(filename):
		create_ucnt_file(ip)
	f = open(filename)
	last_ucnt = f.readline().split(';')[F_UCNT_IDX]
	f.close()
	return last_ucnt		

def write_last_ucnt(ip, ucnt, diff):
	f = open(FILE_UCNT.replace("<ip>", ip), 'w')
	f.write(str(ucnt) + ";" + str(diff))
	f.close()

def read_diff_file(ip):
	filename = FILE_UCNT.replace("<ip>", ip)
	if not os.path.isfile(filename):
		create_ucnt_file(ip)
	f = open(filename)
	diff = f.readline().split(';')[F_DIFF_IDX]
	f.close()
	return diff

def main():
	if len(sys.argv) == 1:
		return

	ip = sys.argv[1]

	if len(sys.argv) == 2:
		# get ucnt from snmp, get last ucnt from file and compare
		ucnt = read_ucnt(ip)
		last_ucnt = read_last_ucnt(ip)
		diff = int(ucnt) - int(last_ucnt)
		write_last_ucnt(ip, ucnt, diff)

	if len(sys.argv) == 3 and sys.argv[2] == "file":
		# only read status from file
		diff = read_diff_file(ip)

	print "Update counter diff = " + str(diff)
	if int(diff) > 0:
		sys.exit(0)
	else:
		sys.exit(2)



if __name__ == '__main__':
	main()
